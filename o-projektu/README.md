---
sidebarDepth: 2
---

# Changelog

[[TOC]]

---
## Verze 2.2.0 v další verzi
Release - neznámo

**Plán**
- Knihovna na správu závodů/tréninků událostí obecně. Mělo by být napíchnuto na 


**Upraveno**
- [Frontent] - pokud jsi přihlášený a máš právo Redaktora, můžeš rovnou editovat novinku z veřejné části stránek
- [CSS] - opraveny chyby v css-ku 

## Verze 2.1.0
Release 19.8.2018

Upgrade z verze Laravelu 5.7 na 5.8

**Přidáno**
- Knihovna naminpulace s obrázky (http://image.intervention.io/)

**Upraveno**
- [Média] - přidána akce resize obrázků na maximální šířku 1200px po uploadu obrázků na server
- [Média] - přidáno vytváření náhledů obrázků _thumb velikosti 640px


## Verze 2.0.0 alfa

První release, přepsané aplikace do Laravelu 5.7 s použitím TalwindCss a Vue.js