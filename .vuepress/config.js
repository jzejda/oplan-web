// .vuepress/config.js
module.exports = {
    title: 'oPlan',
    description: 'Dokumentace k projektu - orienteering planner v2.2.1',
    head: [
        ['link', { rel: 'icon', href: 'images/favicon.ico' }]
    ],
    /*base: '/oplan/',*/

    themeConfig: {
        logo: 'images/oplogo.png',
        sidebarDepth: 2,
        nav: [
            { text: 'Úvod', link: '/' },
            { text: 'Příručka uživatel', link: '/prirucka-uzivatele/' },
            {
                text: 'Chtěl bych pomoct',
                items: [
                    { text: 'Chci vyvíjet', items: [
                            { text: 'Co by to obnášelo', link: '/prirucka-vyvojare/' },
                            { text: 'Popis prostředí', link: '/prirucka-vyvojare/popis-prostredi' },
                        ] },
                    { text: 'Chci pomoct', items: [
                            { text: 'Jak na to', link: '/' },
                            { text: 'Guide', link: '/guide/' },
                        ] },
                    { text: 'O projektu', items: [
                            { text: 'Changelog', link: '/o-projektu/' },
                            { text: 'Průvodce aktualizací', link: '/o-projektu/pruvodce-aktualizaci.md' },
                        ] }
                ]
            },

            { text: 'Kontakt', link: '/about' },

            { text: 'GitLab', link: 'https://gitlab.com/jzejda/oplan' },
        ],
        sidebar: {
            '/prirucka-uzivatele/': [
                '',
                'prihlaseni',
                'prostredi-administrace',
                'muj-ucet',
                'redaktor',
                'uzivatele'
            ],

            '/prirucka-vyvojare/': [
                '',
                'popis-prostredi',
                'vyvojove-prostredi'
            ],

            // fallback
            '/': [
                '',        /* / */
                'about'    /* /about.html */
            ]
        }
    },
};
